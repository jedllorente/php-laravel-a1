<?php

function getFullAddress($country, $city, $province, $specificAddress) {
    return "$specificAddress, $city, $province, $country";
}


function getLetterGrade($grades){
    if ($grades >= 98 and $grades <= 100) {
        return "$grades is equivalent to A+"; 
    } else if ($grades >= 95 and $grades <= 97) {
        return "$grades is equivalent to A";
    } else if ($grades >= 92 and $grades <= 94) {
        return "$grades is equivalent to A-";
    } else if ($grades >= 89 and $grades <= 91) {
        return "$grades is equivalent to B+";
    } else if ($grades >= 86 and $grades <= 88) {
        return "$grades is equivalent to B";
    } else if ($grades >= 83 and $grades <= 85) {
        return "$grades is equivalent to B-";
    } else if ($grades >= 80 and $grades <= 82) {
        return "$grades is equivalent to C+";
    } else if ($grades >= 77 and $grades <= 79) {
        return "$grades is equivalent to C";
    } else if ($grades >= 75 and $grades <= 76) {
        return "$grades is equivalent to C-";
    } else if ($grades < 75){
        return "$grades is equivalent to Failed";
    }   
}


?>